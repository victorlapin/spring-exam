package ru.spsr.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.spsr.spring.model.MemberState;
import ru.spsr.spring.model.Project;
import ru.spsr.spring.model.Team;
import ru.spsr.spring.model.TeamMember;
import ru.spsr.spring.service.BaseService;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping(value = "/api", produces = "application/json")
public class RestController {
    @Autowired
    BaseService service;

    @PostConstruct
    public void initData() {
        service.initData();
    }

    @RequestMapping(produces = "text/plain")
    public String ping() {
        return "Works";
    }

    @RequestMapping(value = "/teams")
    public List<Team> getAllTeams() {
        final List<Team> result = service.getAllTeams();
        for (Team team : result) {
            team.setMembers(null);
        }
        return result;
    }

    @RequestMapping(value = "/teams/{id}")
    public Team getTeamById(@PathVariable long id) {
        final Team result = service.getTeamById(id);
        result.setMembers(null);
        return result;
    }

    @RequestMapping(value = "/members")
    public List<TeamMember> getAllMembers() {
        final List<TeamMember> result = service.getAllMembers();
        for (TeamMember member : result) {
            member.setTeam(null);
            member.setProjects(null);
        }
        return result;
    }

    @RequestMapping(value = "/members/{id}")
    public TeamMember getMemberById(@PathVariable long id) {
        final TeamMember result = service.getMemberById(id);
        result.setTeam(null);
        result.setProjects(null);
        return result;
    }

    @RequestMapping(value = "/members/state/{state}")
    public List<TeamMember> getMembersByState(@PathVariable String state) {
        final MemberState mstate = MemberState.valueOf(state.toUpperCase());
        final List<TeamMember> result = service.getMembersByState(mstate);
        for (TeamMember member : result) {
            member.setTeam(null);
            member.setProjects(null);
        }
        return result;
    }

    @RequestMapping(value = "/projects")
    public List<Project> getAllProjects() {
        final List<Project> result = service.getAllProjects();
        for (Project project : result) {
            project.setMembers(null);
        }
        return result;
    }

    @RequestMapping(value = "/projects/{id}")
    public Project getProjectById(@PathVariable long id) {
        final Project result = service.getProjectById(id);
        result.setMembers(null);
        return result;
    }
}
