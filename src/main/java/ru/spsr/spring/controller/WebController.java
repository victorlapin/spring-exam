package ru.spsr.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.spsr.spring.model.Team;
import ru.spsr.spring.model.TeamMember;
import ru.spsr.spring.service.BaseService;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Controller
public class WebController {
    @Autowired
    BaseService service;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "redirect:/teams";
    }

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    public String teams(Model model) {
        model.addAttribute("teams", service.getAllTeams());
        return "teams";
    }

    @RequestMapping(value = "/members", method = RequestMethod.GET)
    public String members(@RequestParam(value = "team_id", required = false) final Long teamId, Model model) {
        if (teamId == null) {
            return "redirect:";
        } else {
            final Team team = service.getTeamById(teamId);
            if (team != null) {
                model.addAttribute("members", team.getMembers());
                return "members";
            } else {
                return "redirect:/error";
            }
        }
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public String projects(@RequestParam(value = "member_id", required = false) final Long memberId, Model model) {
        if (memberId == null) {
            return "redirect:";
        } else {
            final TeamMember member = service.getMemberById(memberId);
            if (member != null) {
                model.addAttribute("projects", member.getProjects());
                return "projects";
            } else {
                return "redirect:/error";
            }
        }
    }
}
