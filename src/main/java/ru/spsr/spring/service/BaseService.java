package ru.spsr.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.spsr.spring.dao.ProjectDaoImpl;
import ru.spsr.spring.dao.TeamDaoImpl;
import ru.spsr.spring.dao.TeamMemberDaoImpl;
import ru.spsr.spring.model.MemberState;
import ru.spsr.spring.model.Project;
import ru.spsr.spring.model.Team;
import ru.spsr.spring.model.TeamMember;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Service
public class BaseService {
    @Autowired
    TeamDaoImpl teamDao;

    @Autowired
    TeamMemberDaoImpl teamMemberDao;

    @Autowired
    ProjectDaoImpl projectDao;

    @Transactional
    public void initData() {
        final Team team = new Team();
        team.setName("SlimRoms");
        team.setAddress("Somewhere in UK");
        teamDao.save(team);

        final Project prjRecents = new Project();
        prjRecents.setName("Slim Recents");
        projectDao.save(prjRecents);
        final Project prjBullhead = new Project();
        prjBullhead.setName("Nexus 5X maintenance");
        projectDao.save(prjBullhead);
        final Project prjSlimTE = new Project();
        prjSlimTE.setName("Slim Theme Engine");
        projectDao.save(prjSlimTE);
        final Project prjAngler = new Project();
        prjAngler.setName("Nexus 6P maintenance");
        projectDao.save(prjAngler);

        final TeamMember tmVictor = new TeamMember();
        tmVictor.setName("Victor Lapin");
        tmVictor.setCountry("Russia");
        tmVictor.setState(MemberState.ACTIVE);
        tmVictor.setTeam(team);
        teamMemberDao.save(tmVictor);
        final TeamMember tmGriffin = new TeamMember();
        tmGriffin.setName("Griffin Millender");
        tmGriffin.setCountry("USA");
        tmGriffin.setState(MemberState.ACTIVE);
        tmGriffin.setTeam(team);
        teamMemberDao.save(tmGriffin);
        final TeamMember tmJP = new TeamMember();
        tmJP.setName("Jean-Pierre Rasquin");
        tmJP.setCountry("Luxembourg");
        tmJP.setState(MemberState.ACTIVE);
        tmJP.setTeam(team);
        teamMemberDao.save(tmJP);
        final TeamMember tmLars = new TeamMember();
        tmLars.setName("Lars Greiss");
        tmLars.setCountry("Germany");
        tmLars.setState(MemberState.RETIRED);
        tmLars.setTeam(team);
        teamMemberDao.save(tmLars);

        List<Project> projects = new ArrayList<>();
        projects.add(prjBullhead);
        projects.add(prjSlimTE);
        tmVictor.setProjects(projects);

        projects = new ArrayList<>();
        projects.add(prjRecents);
        projects.add(prjRecents);
        tmGriffin.setProjects(projects);

        projects = new ArrayList<>();
        projects.add(prjAngler);
        tmJP.setProjects(projects);

        List<TeamMember> members = new ArrayList<>();
        members.add(tmVictor);
        prjBullhead.setMembers(members);

        members = new ArrayList<>();
        members.add(tmGriffin);
        prjRecents.setMembers(members);

        members = new ArrayList<>();
        members.add(tmVictor);
        members.add(tmGriffin);
        prjSlimTE.setMembers(members);

        members = new ArrayList<>();
        members.add(tmJP);
        prjAngler.setMembers(members);

        members = new ArrayList<>();
        members.add(tmVictor);
        members.add(tmGriffin);
        members.add(tmJP);
        team.setMembers(members);
    }

    public List<Team> getAllTeams() {
        return teamDao.findAll();
    }

    public Team getTeamById(final long id) {
        return teamDao.findById(id);
    }

    public List<TeamMember> getAllMembers() {
        return teamMemberDao.findAll();
    }

    public TeamMember getMemberById(final long id) {
        return teamMemberDao.findById(id);
    }

    public List<TeamMember> getMembersByState(final MemberState state) {
        return teamMemberDao.findByState(state);
    }

    public List<Project> getAllProjects() {
        return projectDao.findAll();
    }

    public Project getProjectById(final long id) {
        return projectDao.findById(id);
    }
}
