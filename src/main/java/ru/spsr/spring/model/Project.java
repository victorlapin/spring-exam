package ru.spsr.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Entity
@Table(name = "Project")
public class Project extends AbstractEntity implements Serializable {
    @ManyToMany
    private List<TeamMember> members;

    public List<TeamMember> getMembers() {
        return members;
    }

    public void setMembers(List<TeamMember> members) {
        this.members = members;
    }
}
