package ru.spsr.spring.model;

/**
 * Created by lapin_vm on 14.02.2017.
 */
public enum MemberState {
    ACTIVE,
    RETIRED
}
