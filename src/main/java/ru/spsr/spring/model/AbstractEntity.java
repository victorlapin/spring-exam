package ru.spsr.spring.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_gen")
    @TableGenerator(name = "entity_gen", table = "Id_Generator", pkColumnName = "Entity_Type", valueColumnName = "Last_Id", allocationSize = 1)
    protected long id;

    @Column(name = "Name", nullable = false)
    protected String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
