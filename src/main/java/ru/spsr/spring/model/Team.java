package ru.spsr.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Entity
@Table(name = "Team")
public class Team extends AbstractEntity implements Serializable {
    @Column(name = "Address")
    private String address;

    @OneToMany(mappedBy = "team")
    private List<TeamMember> members;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<TeamMember> getMembers() {
        return members;
    }

    public void setMembers(List<TeamMember> members) {
        this.members = members;
    }
}
