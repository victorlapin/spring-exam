package ru.spsr.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Entity
@Table(name = "Team_Member")
public class TeamMember extends AbstractEntity implements Serializable {
    @Column(name = "Country", nullable = true, length = 100)
    private String country;

    @Enumerated(value = EnumType.STRING)
    private MemberState state;

    @ManyToOne
    private Team team;

    @ManyToMany(mappedBy = "members")
    private List<Project> projects;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public MemberState getState() {
        return state;
    }

    public void setState(MemberState state) {
        this.state = state;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
