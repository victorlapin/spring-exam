package ru.spsr.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by lapin_vm on 16.02.2017.
 */
@Aspect
@Component
public class LoggingAspect {
    private final static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("execution(* ru.spsr.spring..*.*(..))")
    public Object logDaoCall(ProceedingJoinPoint joinPoint) throws Throwable {
        final String methodName = joinPoint.getSignature().getName();
        logger.info(methodName + " executing");
        Object result = joinPoint.proceed();
        logger.info(methodName + " completed");

        return result;
    }
}
