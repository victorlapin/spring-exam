package ru.spsr.spring.dao;

import org.springframework.stereotype.Component;
import ru.spsr.spring.model.Project;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Component
public class ProjectDaoImpl extends AbstractDao<Project> implements ProjectDao {
    public ProjectDaoImpl() {
        setClazz(Project.class);
    }
}
