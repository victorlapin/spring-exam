package ru.spsr.spring.dao;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Component
public abstract class AbstractDao<T extends Serializable> {
    private Class<T> clazz;

    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractDao() { }

    protected void setClazz(final Class<T> clazz) {
        this.clazz = clazz;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery("from " + clazz.getName(), clazz).getResultList();
    }

    public T findById(final long id) {
        return entityManager.find(clazz, id);
    }

    public void save(final T entity) {
        entityManager.persist(entity);
    }

    public void update(final T entity) {
        entityManager.merge(entity);
    }

    public void delete(final T entity) {
        entityManager.remove(entity);
    }
}
