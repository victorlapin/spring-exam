package ru.spsr.spring.dao;

import org.springframework.stereotype.Component;
import ru.spsr.spring.model.Team;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Component
public class TeamDaoImpl extends AbstractDao<Team> implements TeamDao {
    public TeamDaoImpl() {
        setClazz(Team.class);
    }
}
