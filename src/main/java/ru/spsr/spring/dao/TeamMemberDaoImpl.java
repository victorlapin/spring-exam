package ru.spsr.spring.dao;

import org.springframework.stereotype.Component;
import ru.spsr.spring.model.MemberState;
import ru.spsr.spring.model.TeamMember;

import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
@Component
public class TeamMemberDaoImpl extends AbstractDao<TeamMember> implements TeamMemberDao {
    public TeamMemberDaoImpl() {
        setClazz(TeamMember.class);
    }

    @Override
    public List<TeamMember> findByState(final MemberState state) {
        return entityManager.createQuery("from " + TeamMember.class.getName() + " where state = :state", TeamMember.class)
                .setParameter("state", state)
                .getResultList();
    }
}
