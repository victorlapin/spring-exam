package ru.spsr.spring.dao;

import ru.spsr.spring.model.MemberState;
import ru.spsr.spring.model.TeamMember;

import java.util.List;

/**
 * Created by lapin_vm on 14.02.2017.
 */
public interface TeamMemberDao {
    List<TeamMember> findByState(final MemberState state);
}
