<%--
  Created by IntelliJ IDEA.
  User: lapin_vm
  Date: 15.02.2017
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><spring:message code="message.teams"/></title>
</head>
<body>
<h2><spring:message code="message.teams"/></h2>
<ul>
    <c:forEach items="${teams}" var="team">
        <li>
            <a href="<spring:url value="/members?team_id=${team.id}" />">${team.name}</a>
        </li>
    </c:forEach>
</ul>
</body>
</html>
