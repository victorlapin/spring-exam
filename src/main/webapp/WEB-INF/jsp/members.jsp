<%--
  Created by IntelliJ IDEA.
  User: lapin_vm
  Date: 15.02.2017
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><spring:message code="message.members"/></title>
</head>
<body>
<h2><spring:message code="message.members"/></h2>
<ul>
    <c:forEach items="${members}" var="member">
        <li>
            <a href="<spring:url value="/projects?member_id=${member.id}" />">${member.name}
                (${member.country}) - ${member.state}</a>
        </li>
    </c:forEach>
</ul>
</body>
</html>
